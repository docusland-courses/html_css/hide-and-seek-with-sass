# Découverte de SASS

L'objectif de cet exercice est de s'amuser un peu avec les boucles au sein de SASS. 


## Consignes

Vous avez au sein de ce dépôt un fichier html de base `index.html`. 
Ce dernier affiche l'image d'une fille en arrière plan, et génère au sein d'un bloc `to_scratch` un ensemble de divs par le biais d'un petit script javascript intégré. 

Réalisez le fichier SCSS permettant d'afficher le bloc to_scratch ainsi que tous les divs enfants par dessus l'image. 

L'objectif ici est d'avoir un damier de blocs au dessus de l'image. Ces blocs ayant une couleur aléatoire et cachant l'image en dessous.

Puis d'avoir un effet lors du survol de la souris qui masque le div et révèle durant un court instant l'image située en dessous. 

## Utilisation de SASS
Vous disposez au sein de ce dépôt d'un fichier `package.json`.
Ce dernier contient deux run scripts permettant de compiler un fichier `input.scss` pour en générer un fichier `output.css`.

### Installation du dépôt
> npm install

### Execution `one shot` du pré processeur SASS
> npm run compile

### Execution `a la volée` du pré processeur SASS
> npm run watch